package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;

@Service
public class CustomerService {

    Customer cus1 = new Customer("Khanh");
    Customer cus2 = new Customer("Minh Khanh");
    Customer cus3 = new Customer("Trần Khanh");

    public ArrayList<Customer> getCustomerList(){

        ArrayList<Customer> allCus = new ArrayList<>();

        allCus.add(cus1);
        allCus.add(cus2);
        allCus.add(cus3);

        return allCus ;
    }

}
