package com.example.demo.Service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Model.Visit;
@Service
public class VisitService {
    @Autowired
    private CustomerService customerService ;
    Visit visit1 = new Visit("ngày hiện tại" , new Date());
    Visit visit2 = new Visit("ngày tương lai", new Date());
    Visit visit3 = new Visit("ngày quá khứ" , new Date());

    public ArrayList<Visit> getVisitList(){

        ArrayList<Visit> allVisit = new ArrayList<>();

        visit1.setCustomer(customerService.cus1);
        visit2.setCustomer(customerService.cus2);
        visit3.setCustomer(customerService.cus3);

        allVisit.add(visit1);
        allVisit.add(visit2);
        allVisit.add(visit3);

        return allVisit ;
    }
}
